package alexlmad.banregiolibrosapp.Activities;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.R;
import alexlmad.banregiolibrosapp.Services.MySingleton;
import alexlmad.banregiolibrosapp.Services.PostAddBook;
import alexlmad.banregiolibrosapp.Utilities;

public class AddBookActivity extends AppCompatActivity {
    private Activity activity;
    private EditText editText_title;
    private EditText editText_isbn;
    private EditText editText_author_first_name;
    private EditText editText_author_last_name;
    private EditText editText_category;
    private EditText editText_date;
    private EditText editText_pages;
    private EditText editText_editorial;
    private EditText editText_url;
    private EditText editText_description;
    private Button button_add;
    private int dayOfMonth = 0, month = 0, year = 0;
    private double releaseSeconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity = this;
        inflateViews();
        initializeListeners();


    }

    private void inflateViews() {
        editText_title = findViewById(R.id.editText_title);
        editText_isbn = findViewById(R.id.editText_isbn);
        editText_author_first_name = findViewById(R.id.editText_author_first_name);
        editText_author_last_name = findViewById(R.id.editText_author_last_name);
        editText_category = findViewById(R.id.editText_category);
        editText_date = findViewById(R.id.editText_date);
        editText_pages = findViewById(R.id.editText_pages);
        editText_editorial = findViewById(R.id.editText_editorial);
        editText_url = findViewById(R.id.editText_url);
        editText_description = findViewById(R.id.editText_description);
        button_add = findViewById(R.id.button_add);

    }

    private void initializeListeners() {
        button_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields()) {
                    addBook();
                }
            }
        });
        editText_date.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (MotionEvent.ACTION_UP == event.getAction())
                    showDatePicker(); // Instead of your Toast
                return false;
            }
        });

        editText_author_first_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int i, KeyEvent event) {
                if (i == 5 || i == 6) {
                    Utilities.showKeyboard(activity, editText_author_last_name);
                    return true;
                } else return false;
            }
        });
        editText_pages.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int i, KeyEvent event) {
                if (i == 5 || i == 6) {
                    Utilities.showKeyboard(activity, editText_editorial);
                    return true;
                } else return false;
            }
        });
        editText_author_last_name.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int i, KeyEvent event) {
                if (i == 5 || i == 6) {
                    Utilities.showKeyboard(activity, editText_category);
                    return true;
                } else return false;
            }
        });
        editText_category.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int i, KeyEvent event) {
                if (i == 5 || i == 6) {
                    Utilities.hideKeyboard(activity);
                    showDatePicker();
                    return true;
                } else return false;
            }
        });
        editText_isbn.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int i, KeyEvent event) {
                if (i == 5 || i == 6) {
                    Utilities.showKeyboard(activity, editText_author_first_name);
                    return true;
                } else return false;
            }
        });

    }

    private boolean validateFields() {
        boolean response = true;
        editText_title.setError(null);
        editText_isbn.setError(null);
        editText_author_first_name.setError(null);
        editText_author_last_name.setError(null);
        editText_category.setError(null);
        editText_date.setError(null);
        editText_pages.setError(null);
        editText_editorial.setError(null);
        editText_url.setError(null);
        editText_description.setError(null);
        if (editText_title.getText().toString().isEmpty()) {
            editText_title.setError("Es necesario un titulo");
            response = false;
        }
        if (editText_isbn.getText().toString().length() < 13) {
            editText_isbn.setError("No es un ISBN valido");
            response = false;
        }
        if (editText_author_first_name.getText().toString().isEmpty()) {
            editText_author_first_name.setError("Es necesario un nombre de autor");
            response = false;
        }
        if (editText_author_last_name.getText().toString().isEmpty()) {
            editText_author_last_name.setError("Es necesario un nombre de autor");
            response = false;
        }
        if (editText_pages.getText().toString().isEmpty()) {
            editText_pages.setError("Es necesario un número de pagina");
            response = false;
        }
        if (editText_date.getText().toString().isEmpty()) {
            editText_date.setError("Es necesario una fecha de publicación");
            response = false;
        }
        if (editText_category.getText().toString().isEmpty()) {
            editText_category.setError("Es necesario una categoria");
            response = false;
        }
        if (editText_editorial.getText().toString().isEmpty()) {
            editText_editorial.setError("Es necesaria una editorial");
            response = false;
        }
        if (editText_url.getText().toString().isEmpty()) {
            editText_url.setError("Es necesario una url de imagen");
            response = false;
        }
        if (editText_description.getText().toString().isEmpty()) {
            editText_description.setError("Es necesario una descripcion");
            response = false;
        }

        return response;
    }

    private void addBook() {
        PostAddBook.newInstance(activity, getJSONFromBook(), new MySingleton.OnPostAddBookResponseListener() {
            @Override
            public void onGettingData() {

            }

            @Override
            public void onResponse(boolean response, ArrayList<BookModel> books) {
                setResult(RESULT_OK);
                finish();
            }
        });
    }

    private JSONObject getJSONFromBook() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("_id", 0);
            jsonObject.put("isbn", editText_isbn.getText().toString());
            jsonObject.put("title", editText_title.getText().toString());

            JSONObject jsonAuthor = new JSONObject();
            jsonAuthor.put("first_name", editText_author_first_name.getText().toString());
            jsonAuthor.put("last_name", editText_author_last_name.getText().toString());

            jsonObject.put("author", jsonAuthor);
            jsonObject.put("category", editText_category.getText().toString());
            jsonObject.put("published", Double.toString(releaseSeconds));
            jsonObject.put("publisher", editText_editorial.getText().toString());
            jsonObject.put("pages", editText_pages.getText().toString());
            jsonObject.put("description", editText_description.getText().toString());
            jsonObject.put("image_url", editText_url.getText().toString());
            jsonObject.put("_createdOn", "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    private double getSecondsFromDate() {
        return 0;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDatePicker() {
        final Calendar calendar = Calendar.getInstance();
        if (dayOfMonth != 0 && month != 0 && year != 0) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, month);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        }
        Calendar calendarMaxDate = Calendar.getInstance();
        Calendar calendarMinDate = Calendar.getInstance();
        calendarMaxDate.add(Calendar.YEAR, 12);
        calendarMinDate.add(Calendar.YEAR, 0);
        calendarMinDate.add(Calendar.MONTH, 0);

        new SpinnerDatePickerDialogBuilder()
                .context(activity)
                .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int _year, int _month, int _dayOfMonth) {
                        year = _year;
                        month = _month;
                        dayOfMonth = _dayOfMonth;


                        Calendar calendarTemp = Calendar.getInstance();
                        calendarTemp.set(Calendar.HOUR_OF_DAY, 0);
                        calendarTemp.set(Calendar.MINUTE, 0);
                        calendarTemp.set(Calendar.SECOND, 0);
                        calendarTemp.set(Calendar.MILLISECOND, 0);

                        calendarTemp.set(Calendar.YEAR, year);
                        calendarTemp.set(Calendar.MONTH, month);
                        calendarTemp.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        /////
                        //
                        // ////////////// UPDATE T EXT /////////////////
                        String myFormat = "yyyy-MM-dd"; //In which you need put here
                        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                        String stringMonth = Integer.toString(month + 1);
                        String stringDay = Integer.toString(dayOfMonth);
                        String stringYear = Integer.toString(year);
                        stringMonth = month + 1 > 9 ? stringMonth : "0" + stringMonth;
                        editText_date.setText(stringDay + "/" + stringMonth + "/" + year);
                        releaseSeconds = calendarTemp.getTimeInMillis() / 1000;
                        Log.d("releaseSeconds ", Integer.toString((int) releaseSeconds));
                        Utilities.showKeyboard(activity, editText_pages);
                    }
                })
                .showTitle(false)
                .showDaySpinner(true)
                .defaultDate(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
                /* .maxDate(calendarMaxDate.get(Calendar.YEAR), calendarMaxDate.get(Calendar.MONTH), calendarMaxDate.get(Calendar.DAY_OF_MONTH))
                 .minDate(calendarMinDate.get(Calendar.YEAR), 0, 1)
                 */.build()
                .show();
    }
}
