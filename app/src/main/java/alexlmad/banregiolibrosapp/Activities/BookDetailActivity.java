package alexlmad.banregiolibrosapp.Activities;

import android.app.Activity;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.R;
import alexlmad.banregiolibrosapp.Utilities;

public class BookDetailActivity extends AppCompatActivity {
    private Activity activity;
    private BookModel book;

    private TextView textView_title;
    private TextView textView_author;
    private ImageView imageView;
    private TextView textView_category;
    private TextView textView_isnb;
    private TextView textView_date;
    private TextView textView_pages;
    private TextView textView_description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity = this;
        book = (BookModel) getIntent().getSerializableExtra("book");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        inflateViews();
        initializeListeners();
        initializeData();

    }

    private void initializeListeners() {

    }

    private void inflateViews() {

        textView_title = findViewById(R.id.textView_title);
        textView_author = findViewById(R.id.textView_author);
        imageView = findViewById(R.id.imageView);
        textView_category = findViewById(R.id.textView_category);
        textView_isnb = findViewById(R.id.textView_isnb);
        textView_date = findViewById(R.id.textView_date);
        textView_pages = findViewById(R.id.textView_pages);
        textView_description = findViewById(R.id.textView_description);
        textView_date.setText(Utilities.getDateFromSeconds(book.getPublished()));
    }

    private void initializeData() {
        textView_title.setText(book.getTitle());
        textView_author.setText(book.getAuthorName());
        textView_category.setText(book.getCategory());
        textView_isnb.setText(book.getIsbn());
        textView_pages.setText(Integer.toString(book.getPages()) + " " + getResources().getString(R.string.pages));
        textView_description.setText(book.getDescription());
        if (!book.getImageUrl().isEmpty())
            Picasso.get().load(book.getImageUrl()).into(imageView);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

}
