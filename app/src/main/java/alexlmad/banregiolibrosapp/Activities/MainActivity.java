package alexlmad.banregiolibrosapp.Activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.util.ArrayList;

import alexlmad.banregiolibrosapp.Adapters.BooksAdapter;
import alexlmad.banregiolibrosapp.Adapters.CustomExpandableListAdapter;
import alexlmad.banregiolibrosapp.Models.AuthorModel;
import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.Models.ParentBookModel;
import alexlmad.banregiolibrosapp.OnRecyclerClickListener;
import alexlmad.banregiolibrosapp.R;
import alexlmad.banregiolibrosapp.SQLiteHelper;
import alexlmad.banregiolibrosapp.Services.DeleteBook;
import alexlmad.banregiolibrosapp.Services.GetBooks;
import alexlmad.banregiolibrosapp.Services.MySingleton;
import alexlmad.banregiolibrosapp.Utilities;

import static android.view.View.GONE;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int ADD_BOOK = 1000;
    private Activity activity;
    private CustomExpandableListAdapter adapter;
    private ArrayList<ParentBookModel> booksAux;
    private ArrayList<ParentBookModel> books;

    private FloatingActionButton fab_add_book;
    private RelativeLayout relativeLayout_toolBar;
    private EditText editText_search;
    private ImageView imageView_search;
    private ImageView imageView_back_search;
    private ExpandableListView expandableListView;
    private CustomExpandableListAdapter.OnChildClickListener onChildClickListener;


    private Animation animation = null;

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == ADD_BOOK)
            getBooks();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity = this;
        SQLiteHelper.initialize();


        inflateViews();
        initializeListners();
        getBooks();
    }

    private void getBooks() {
        GetBooks.newInstance(activity, new MySingleton.OnGetBooksResponseListener() {
            @Override
            public void onGettingData() {
                Log.d("GetBooks", "onGettingData");
            }

            @Override
            public void onResponse(boolean response, ArrayList<ParentBookModel> _parenBooks) {
                Log.d("GetBooks", "onResponse");
                if (response) {
                    booksAux = _parenBooks;
                    books = _parenBooks;
                }

                adapter = new CustomExpandableListAdapter(activity, books, onChildClickListener);
                expandableListView.setAdapter(adapter);
            }
        });
    }

    private void initializeListners() {
        onChildClickListener = new CustomExpandableListAdapter.OnChildClickListener() {
            @Override
            public void onChildClick(int groupPosition, int childPosition) {
                Intent intent = new Intent(activity, BookDetailActivity.class);
                intent.putExtra("book", books.get(groupPosition).getBooks().get(childPosition));
                startActivity(intent);
            }

            @Override
            public boolean onChildLongClick(int groupPosition, int childPosition) {
                showDeleteDialog(groupPosition, childPosition);
                return false;
            }
        };

        imageView_search.setOnClickListener(this);
        imageView_back_search.setOnClickListener(this);
        fab_add_book.setOnClickListener(this);
        editText_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                applyFilter();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        editText_search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus)
                    editText_search.post(new Runnable() {
                        @Override
                        public void run() {
                            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.showSoftInput(editText_search, InputMethodManager.SHOW_IMPLICIT);
                        }
                    });
            }
        });
    }

    private void applyFilter() {
        String search = editText_search.getText().toString().toLowerCase();
        if (search.equalsIgnoreCase("")) {
            books = new ArrayList<>(booksAux);
            adapter = new CustomExpandableListAdapter(activity, books, onChildClickListener);
            expandableListView.setAdapter(adapter);
            // recyclerView.setAdapter(booksAdapter);
        } else {
            books = new ArrayList<>();
            for (ParentBookModel parentBook : booksAux) {
                ArrayList<BookModel> booksTemp = new ArrayList<>();
                for (BookModel book : parentBook.getBooks()) {
                    if (book.getIsbn().toLowerCase().contains(search))
                        booksTemp.add(book);
                    else if (book.getTitle().toLowerCase().contains(search))
                        booksTemp.add(book);
                    else if (book.getAuthor().getFirstName().toLowerCase().contains(search))
                        booksTemp.add(book);
                    else if (book.getAuthor().getLastName().toLowerCase().contains(search)) {
                        booksTemp.add(book);
                    }
                }
                if (booksTemp.size() != 0)
                    books.add(new ParentBookModel(booksTemp.get(0).getCategory(), booksTemp));

            }
            adapter = new CustomExpandableListAdapter(activity, books, onChildClickListener);
            expandableListView.setAdapter(adapter);
        }
    }

    private void inflateViews() {
        fab_add_book = findViewById(R.id.fab_add_book);
        relativeLayout_toolBar = findViewById(R.id.relativeLayout_toolBar);
        editText_search = findViewById(R.id.editText_search);
        imageView_search = findViewById(R.id.imageView_search);
        imageView_back_search = findViewById(R.id.imageView_back_search);
        expandableListView = findViewById(R.id.expandableListView);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.imageView_back_search:
                onClearSearch();
                break;

            case R.id.imageView_search:
                animation = AnimationUtils.loadAnimation(activity, R.anim.slide_right_to_left);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        relativeLayout_toolBar.setVisibility(GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                relativeLayout_toolBar.startAnimation(animation);
                editText_search.requestFocus();
                break;
            case R.id.fab_add_book:
                if (Utilities.verifyInternet())
                    startActivityForResult(new Intent(activity, AddBookActivity.class), ADD_BOOK);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (relativeLayout_toolBar.getVisibility() == GONE) {
            onClearSearch();
        }

    }

    private void onClearSearch() {
        editText_search.setText("");
        animation = AnimationUtils.loadAnimation(activity, R.anim.slide_left_to_right);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                relativeLayout_toolBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        relativeLayout_toolBar.startAnimation(animation);
        Utilities.hideKeyboard(activity, editText_search);

    }

    private void showDeleteDialog(final int groupPosition, final int childPosition) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Alerta");
        builder.setMessage("¿Deseas eliminar el libro?");
        builder.setPositiveButton("Si", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DeleteBook.newInstance(activity, books.get(groupPosition).getBooks().get(childPosition).getId(), new MySingleton.OnDeleteBookResponseListener() {
                    @Override
                    public void onGettingData() {

                    }

                    @Override
                    public void onResponse(boolean response) {
                        getBooks();
                    }
                });
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();
    }
}
