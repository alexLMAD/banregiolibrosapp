package alexlmad.banregiolibrosapp.Models;

import java.io.Serializable;

public class    BookModel implements Serializable {
    private String id;
    private String isbn;
    private String title;
    private AuthorModel author;
    private String category;
    private int published;
    private String publisher;
    private int pages;
    private String description;
    private String imageUrl;
    private String createdOn;

    public BookModel() {
    }

    public BookModel(String id, String isbn, String title, AuthorModel author, String category, int published, String publisher, int pages, String description, String imageUrl, String createdOn) {
        this.id = id;
        this.isbn = isbn;
        this.title = title;
        this.author = author;
        this.category = category;
        this.published = published;
        this.publisher = publisher;
        this.pages = pages;
        this.description = description;
        this.imageUrl = imageUrl;
        this.createdOn = createdOn;
    }

    public BookModel(String title, AuthorModel author, String category) {
        this.title = title;
        this.author = author;
        this.category = category;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public AuthorModel getAuthor() {
        return author;
    }

    public void setAuthor(AuthorModel author) {
        this.author = author;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPublished() {
        return published;
    }

    public void setPublished(int published) {
        this.published = published;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getAuthorName() {
        if (author != null)
            return author.getFirstName() + " " + author.getLastName();
        else
            return "";
    }
}
