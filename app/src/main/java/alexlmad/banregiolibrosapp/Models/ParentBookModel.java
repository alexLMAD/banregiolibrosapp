package alexlmad.banregiolibrosapp.Models;

import java.util.ArrayList;

public class ParentBookModel {
    private String title;
    private ArrayList<BookModel> books;

    public ParentBookModel() {
    }

    public ParentBookModel(String title, ArrayList<BookModel> books) {
        this.title = title;
        this.books = books;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<BookModel> getBooks() {
        return books;
    }

    public void setBooks(ArrayList<BookModel> books) {
        this.books = books;
    }
}
