package alexlmad.banregiolibrosapp;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.VolleyError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;

public class Utilities {
    public static boolean verifyInternet() {
        Context context = BanregioLibrosApp.getAppContext();
        try {
            ConnectivityManager connMgr = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
            if (networkInfo != null && networkInfo.isConnected()) {

                return true;
                // fetch data
            } else {
                Toast.makeText(context, context.getResources().getString(R.string.NoInternet), Toast.LENGTH_LONG).show();
                return false;
                // display error
            }
        } catch (Exception e) {
            return false;
        }
    }

    public static String parseVolleyError(VolleyError error) {
        String message = "";
        if (error.networkResponse != null)
            try {
                String responseBody = new String(error.networkResponse.data, "utf-8");
                JSONObject data = new JSONObject(responseBody);
                message = data.getString("error_description");
            } catch (JSONException e) {
                error.printStackTrace();
            } catch (UnsupportedEncodingException errorr) {
                error.printStackTrace();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        else
            message = "";
        return message;
    }

    public static String getDateFromSeconds(double published) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis((long) (published * 1000L));
        String dateString = calendar.get(Calendar.DAY_OF_MONTH) + " de ";
        switch (calendar.get(Calendar.MONTH)) {
            case 1:
                dateString += "Enero";
                break;
            case 2:
                dateString += "Febrero";
                break;
            case 3:
                dateString += "Marzo";
                break;
            case 4:
                dateString += "Abril";
                break;
            case 5:
                dateString += "Mayo";
                break;
            case 6:
                dateString += "Junio";
                break;
            case 7:
                dateString += "Julio";
                break;
            case 8:
                dateString += "Agosto";
                break;
            case 9:
                dateString += "Septiembre";
                break;
            case 10:
                dateString += "Octubre";
                break;
            case 11:
                dateString += "Noviembre";
                break;
            case 12:
                dateString += "Diciembre";
                break;
        }
        dateString += (" de " + calendar.get(Calendar.YEAR));
        return dateString;
    }

    public static void hideKeyboard(Activity activity, EditText editText) {
        editText.clearFocus();
        if (editText != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(editText.getWindowToken(), 0);
        }
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null) {
            InputMethodManager inputManager = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputManager != null)
                inputManager.hideSoftInputFromWindow(activity.getWindow().getDecorView().getApplicationWindowToken(), 0);
            activity.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
    }

    public static void showKeyboard(Activity activity, EditText editText) {
        editText.setFocusableInTouchMode(true);
        editText.requestFocus();
        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
        ((InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE)).toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);

    }

}
