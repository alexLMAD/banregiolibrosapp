package alexlmad.banregiolibrosapp.Services;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import alexlmad.banregiolibrosapp.Models.AuthorModel;
import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.Utilities;

public class PostAddBook {
    private static PostAddBook instance;
    public static final String TAG = "S_POST_BOOK";
    private StringRequest stringRequest;


    public static synchronized PostAddBook newInstance(Context context, JSONObject params, MySingleton.OnPostAddBookResponseListener onResponseListener) {
        if (instance != null)
            instance.stopService();
        instance = new PostAddBook(context, params, onResponseListener);

        return instance;
    }

    private PostAddBook(Context context, JSONObject params, MySingleton.OnPostAddBookResponseListener onResponseListener) {
        onResponseListener.onGettingData();
        getData(context, params, onResponseListener);
    }


    private void getData(Context context, final JSONObject params, final MySingleton.OnPostAddBookResponseListener onResponseListener) {
        String url = _Paths.GET_BOOKS;

        stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.d(TAG, response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    ArrayList<BookModel> books = new ArrayList<>();

                    for (int i = 0; i < jsonArray.length(); i++) {
                        BookModel book = new BookModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        JSONObject authorObject = jsonObject.getJSONObject("author");

                        String id = jsonObject.getString("_id");
                        String isbn = jsonObject.getString("isbn");
                        String title = jsonObject.getString("title");
                        String authorFirstName = authorObject.getString("first_name");
                        String authorLastName = authorObject.getString("last_name");
                        AuthorModel author = new AuthorModel(authorFirstName, authorLastName);
                        String category = jsonObject.getString("category");
                        int published = jsonObject.getInt("published");
                        String publisher = jsonObject.getString("publisher");
                        int pages = jsonObject.getInt("pages");
                        String description = jsonObject.getString("description");
                        String imageURL = jsonObject.getString("image_url");
                        String createdOn = jsonObject.getString("_createdOn");
                        book = new BookModel(id,
                                isbn,
                                title,
                                author,
                                category,
                                published,
                                publisher,
                                pages,
                                description,
                                imageURL,
                                createdOn);
                        books.add(book);
                    }
                    onResponseListener.onResponse(true, books);


                } catch (JSONException e) {
                    e.printStackTrace();
                    onResponseListener.onResponse(false, null);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());

                onResponseListener.onResponse(false, null);
            }

        }) {

            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                String temp = params.toString();
                return temp.getBytes();
            }

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {

                return null;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<String, String>();
                headers.put("Content-Type", "application/json");
                headers.put("Accept", "application/json");
                return headers;
            }
        };
        if (Utilities.verifyInternet())
            MySingleton.getInstance(context)
                    .getRequestQueue()
                    .add(stringRequest).setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    private void stopService() {
        if (stringRequest != null)
            stringRequest.cancel();
    }
}

