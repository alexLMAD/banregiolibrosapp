package alexlmad.banregiolibrosapp.Services;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;

import alexlmad.banregiolibrosapp.Models.AuthorModel;
import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.Models.ParentBookModel;
import alexlmad.banregiolibrosapp.SQLiteHelper;
import alexlmad.banregiolibrosapp.Utilities;


public class GetBooks {
    private static GetBooks instance;
    public static final String TAG = "S_GET_BOOKS";
    private StringRequest stringRequest;


    public static synchronized GetBooks newInstance(Context context, MySingleton.OnGetBooksResponseListener onResponseListener) {
        if (instance != null)
            instance.stopService();
        instance = new GetBooks(context, onResponseListener);

        return instance;
    }

    private GetBooks(Context context, MySingleton.OnGetBooksResponseListener onResponseListener) {
        onResponseListener.onGettingData();
        getData(context, onResponseListener);
    }


    private void getData(Context context, final MySingleton.OnGetBooksResponseListener onResponseListener) {
        String url = _Paths.GET_BOOKS;

        stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.d(TAG, response);
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    ArrayList<BookModel> books = new ArrayList<>();


                    SQLiteHelper.deleteAllData();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        BookModel book = new BookModel();
                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                        JSONObject authorObject = jsonObject.getJSONObject("author");

                        String id = jsonObject.getString("_id");
                        String isbn = jsonObject.getString("isbn");
                        String title = jsonObject.getString("title");
                        String authorFirstName = authorObject.getString("first_name");
                        String authorLastName = authorObject.getString("last_name");
                        AuthorModel author = new AuthorModel(authorFirstName, authorLastName);
                        String category = jsonObject.getString("category");
                        int published = jsonObject.getInt("published");
                        String publisher = jsonObject.getString("publisher");
                        int pages = jsonObject.getInt("pages");
                        String description = jsonObject.getString("description");
                        String imageURL = jsonObject.getString("image_url");
                        String createdOn = jsonObject.getString("_createdOn");
                        book = new BookModel(id,
                                isbn,
                                title,
                                author,
                                category,
                                published,
                                publisher,
                                pages,
                                description,
                                imageURL,
                                createdOn);
                        books.add(book);
                        SQLiteHelper.addData(book);


                    }

                    Collections.sort(books, new Comparator<BookModel>() {
                        @Override
                        public int compare(BookModel b1, BookModel b2) {
                            return b1.getCategory().compareToIgnoreCase(b2.getCategory());
                        }
                    });


                    ArrayList<ParentBookModel> parent = new ArrayList<>();
                    String lastTitle = "";
                    ArrayList<BookModel> booksAux;
                    int parentIndex = 0;
                    for (BookModel book : books) {
                        String currentTitle = book.getCategory();
                        if (lastTitle.equals("")) {
                            booksAux = new ArrayList<BookModel>();
                            lastTitle = currentTitle;
                            booksAux.add(book);
                            parentIndex = 0;
                            parent.add(new ParentBookModel(currentTitle, booksAux));
                        } else {
                            if (currentTitle.equals(lastTitle)) {
                                parent.get(parentIndex).getBooks().add(book);
                                //arrayAscendingOrder(parentIndex);
                            } else {
                                parentIndex++;
                                booksAux = new ArrayList<BookModel>();
                                booksAux.add(book);
                                parent.add(new ParentBookModel(currentTitle, booksAux));
                            }
                        }

                        lastTitle = currentTitle;

                    }

                    onResponseListener.onResponse(true, parent);


                } catch (JSONException e) {
                    e.printStackTrace();
                    onResponseListener.onResponse(false, null);

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());

                onResponseListener.onResponse(false, null);
            }

        });

        if (Utilities.verifyInternet())
            MySingleton.getInstance(context)
                    .getRequestQueue()
                    .add(stringRequest).setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        else
            onResponseListener.onResponse(true, SQLiteHelper.getBooks());
    }

    private void stopService() {
        if (stringRequest != null)
            stringRequest.cancel();
    }


}

