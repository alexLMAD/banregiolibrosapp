package alexlmad.banregiolibrosapp;

import android.database.Cursor;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import alexlmad.banregiolibrosapp.Models.AuthorModel;
import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.Models.ParentBookModel;
import p32929.androideasysql_library.Column;
import p32929.androideasysql_library.EasyDB;

public class SQLiteHelper {
    private static EasyDB easyDB;

    public static void initialize() {
        easyDB = EasyDB.init(BanregioLibrosApp.getAppContext(), "TEST") // "TEST" is the name of the DATABASE
                .setTableName("Book")  // You can ignore this line if you want
                .addColumn(new Column("_id", new String[]{"text", "unique"}))
                .addColumn(new Column("isbn", new String[]{"text", "not null"}))
                .addColumn(new Column("title", new String[]{"text", "not null"}))
                .addColumn(new Column("author_first_name", new String[]{"text", "not null"}))
                .addColumn(new Column("author_last_name", new String[]{"text", "not null"}))
                .addColumn(new Column("category", new String[]{"text", "not null"}))
                .addColumn(new Column("published", new String[]{"text", "not null"}))
                .addColumn(new Column("publisher", new String[]{"text", "not null"}))
                .addColumn(new Column("pages", new String[]{"text", "not null"}))
                .addColumn(new Column("description", new String[]{"text", "not null"}))
                .addColumn(new Column("image_url", new String[]{"text", "not null"}))
                .addColumn(new Column("_createdOn", new String[]{"text", "not null"}))
                .doneTableColumn();
    }

    public static void addData(BookModel book) {
        easyDB.addData("_id", book.getId())
                .addData("isbn", book.getIsbn())
                .addData("title", book.getTitle())
                .addData("author_first_name", book.getAuthor().getFirstName())
                .addData("author_last_name", book.getAuthor().getLastName())
                .addData("category", book.getCategory())
                .addData("published", Integer.toString((int) book.getPublished()))
                .addData("publisher", book.getPublisher())
                .addData("pages", book.getPages())
                .addData("description", book.getDescription())
                .addData("image_url", book.getImageUrl())
                .addData("_createdOn", book.getCreatedOn())
                .doneDataAdding();
    }

    public static void deleteAllData() {
        easyDB.deleteAllDataFromTable();
    }

    public static ArrayList<ParentBookModel> getBooks() {
        ArrayList<BookModel> books = new ArrayList<>();
        Cursor res = easyDB.getAllData();
        int columnIndex = 0;
        while (res.moveToNext()) {
            // Your code here
            BookModel book = new BookModel(
                    res.getString(1),
                    res.getString(2),
                    res.getString(3),
                    new AuthorModel(
                            res.getString(4),
                            res.getString(5)),
                    res.getString(6),
                    res.getInt(7),
                    res.getString(8),
                    res.getInt(9),
                    res.getString(10),
                    res.getString(11),
                    res.getString(12));
            books.add(book);
        }

        Collections.sort(books, new Comparator<BookModel>() {
            @Override
            public int compare(BookModel b1, BookModel b2) {
                return b1.getCategory().compareToIgnoreCase(b2.getCategory());
            }
        });


        ArrayList<ParentBookModel> parent = new ArrayList<>();
        String lastTitle = "";
        ArrayList<BookModel> booksAux;
        int parentIndex = 0;
        for (BookModel book : books) {
            String currentTitle = book.getCategory();
            if (lastTitle.equals("")) {
                booksAux = new ArrayList<BookModel>();
                lastTitle = currentTitle;
                booksAux.add(book);
                parentIndex = 0;
                parent.add(new ParentBookModel(currentTitle, booksAux));
            } else {
                if (currentTitle.equals(lastTitle)) {
                    parent.get(parentIndex).getBooks().add(book);
                    //arrayAscendingOrder(parentIndex);
                } else {
                    parentIndex++;
                    booksAux = new ArrayList<BookModel>();
                    booksAux.add(book);
                    parent.add(new ParentBookModel(currentTitle, booksAux));
                }
            }

            lastTitle = currentTitle;

        }

        return parent;
    }
}
