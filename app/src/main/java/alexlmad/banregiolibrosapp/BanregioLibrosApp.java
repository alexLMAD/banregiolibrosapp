package alexlmad.banregiolibrosapp;

import android.app.Application;
import android.content.Context;

public class BanregioLibrosApp extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        BanregioLibrosApp.context = getApplicationContext();
        // Initialize the Branch object
        //Branch.getAutoInstance(this);
    }

    public static Context getAppContext() {
        return BanregioLibrosApp.context;
    }
}
