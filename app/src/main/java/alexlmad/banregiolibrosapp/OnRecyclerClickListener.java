package alexlmad.banregiolibrosapp;

public interface OnRecyclerClickListener {
    void onClickListener(int viewId, int position);
}
