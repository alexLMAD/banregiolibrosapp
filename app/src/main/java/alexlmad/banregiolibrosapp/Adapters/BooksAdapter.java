package alexlmad.banregiolibrosapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import alexlmad.banregiolibrosapp.Models.AuthorModel;
import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.OnRecyclerClickListener;
import alexlmad.banregiolibrosapp.R;

public class BooksAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<BookModel> items;
    private OnRecyclerClickListener onRecyclerClickListener;

    public BooksAdapter(Context context, ArrayList<BookModel> items, OnRecyclerClickListener onRecyclerClickListener) {
        this.context = context;
        this.items = items;
        this.onRecyclerClickListener = onRecyclerClickListener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_book, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        BookModel book = items.get(position);
        AuthorModel author = book.getAuthor();
        viewHolder.setPosition(position);

        viewHolder.textView_title.setText(book.getTitle());
        viewHolder.textView_author.setText(author.getFirstName() + " " + author.getLastName());
        viewHolder.textView_category.setText(book.getCategory());
        if (!book.getImageUrl().isEmpty())
            Picasso.get().load(book.getImageUrl()).into(viewHolder.imageView);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    private class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private int position = 0;
        private RelativeLayout relativeLayout_root;
        protected ImageView imageView;
        protected TextView textView_title;
        protected TextView textView_author;
        protected TextView textView_category;

        public ViewHolder(@NonNull View convertView) {
            super(convertView);
            imageView = convertView.findViewById(R.id.imageView);
            textView_title = convertView.findViewById(R.id.textView_title);
            textView_author = convertView.findViewById(R.id.textView_author);
            textView_category = convertView.findViewById(R.id.textView_category);
            relativeLayout_root = convertView.findViewById(R.id.relativeLayout_root);
            relativeLayout_root.setOnClickListener(this);
        }


        @Override
        public void onClick(View v) {
            onRecyclerClickListener.onClickListener(v.getId(), position);
        }

        public void setPosition(int position) {
            this.position = position;
        }
    }


}
