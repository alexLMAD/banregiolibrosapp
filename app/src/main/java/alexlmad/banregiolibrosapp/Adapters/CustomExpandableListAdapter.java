package alexlmad.banregiolibrosapp.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import alexlmad.banregiolibrosapp.Models.BookModel;
import alexlmad.banregiolibrosapp.Models.ParentBookModel;
import alexlmad.banregiolibrosapp.R;

public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<ParentBookModel> booksParent;
    private OnChildClickListener onChildClickListener;

    public CustomExpandableListAdapter(Context context, ArrayList<ParentBookModel> booksParent, OnChildClickListener onChildClickListener) {
        this.context = context;
        this.booksParent = booksParent;
        this.onChildClickListener = onChildClickListener;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return booksParent.get(groupPosition).getBooks().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        BookModel book = booksParent.get(groupPosition).getBooks().get(childPosition);
        ViewHolder viewHolder = new ViewHolder();
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_book, null, false);
            viewHolder.relativeLayout_root = convertView.findViewById(R.id.relativeLayout_root);
            viewHolder.imageView = convertView.findViewById(R.id.imageView);
            viewHolder.textView_title = convertView.findViewById(R.id.textView_title);
            viewHolder.textView_author = convertView.findViewById(R.id.textView_author);
            viewHolder.textView_isbn = convertView.findViewById(R.id.textView_isbn);


            convertView.setTag(viewHolder);
        } else
            viewHolder = (ViewHolder) convertView.getTag();

        viewHolder.relativeLayout_root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onChildClickListener.onChildClick(groupPosition, childPosition);
            }
        });
        viewHolder.relativeLayout_root.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                onChildClickListener.onChildLongClick(groupPosition, childPosition);
                return false;
            }
        });
        if (!book.getImageUrl().isEmpty())
            Picasso.get().load(book.getImageUrl()).into(viewHolder.imageView);
        viewHolder.textView_title.setText(book.getTitle());
        viewHolder.textView_author.setText(book.getAuthorName());
        viewHolder.textView_isbn.setText(book.getIsbn());


        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return booksParent.get(groupPosition).getBooks().size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.booksParent.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.booksParent.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ParentBookModel parentBook = booksParent.get(groupPosition);
        HeaderHolder headerHolder = new HeaderHolder();

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.header_book, null, false);
            headerHolder.textView = convertView.findViewById(R.id.textView);
            convertView.setTag(headerHolder);
        } else
            headerHolder = (HeaderHolder) convertView.getTag();

        headerHolder.textView.setText(parentBook.getTitle());

        ExpandableListView elv = (ExpandableListView) parent;
        elv.expandGroup(groupPosition);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    private class ViewHolder {
        RelativeLayout relativeLayout_root;
        ImageView imageView;
        TextView textView_title;
        TextView textView_author;
        TextView textView_isbn;
    }

    private class HeaderHolder {
        TextView textView;
    }

    public interface OnChildClickListener {
        void onChildClick(int groupPosition, int childPosition);

        boolean onChildLongClick(int groupPosition, int childPosition);
    }
}
